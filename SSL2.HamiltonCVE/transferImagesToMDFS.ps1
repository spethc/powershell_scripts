﻿$a  = (Get-Date).AddMinutes(-30).ToString('yyyyMMdd hh:mm:ss')


#Use logger now:
QIALog "performing cleanup:";

function RemoveItem($path, $filter, $timespan){
    QIALog "Clean up '$path' with filter '$filter'" 
    Get-ChildItem $path -Filter $filter -recurse |?{$_.PSIsContainer -eq $false }|?{
        if ($now - $_.LastWriteTime -gt $timespan){
            QIALog "Delete $_";
            $s = $_.FullName.replace("[", "``[").replace("]", "``]");
            $log = "Remove-Item $s"
            Remove-Item $s;
        }
    }
}


function MoveItem ($srcPath, $destPath, $filter, $timespan){
    QIALog "Moving to $destPath";
    New-Item -ItemType directory -Path $destPath;
    Get-ChildItem $srcPath -Filter $filter -Recurse | Where-Object {
        $now - $_.LastWriteTime -gt $timespan
    } |?{
        $s =$_.FullName;
        #https://stackoverflow.com/questions/21008180/copy-file-with-square-brackets-in-the-filename-and-use-wildcard
        $n = $destPath + $_.Name;
        $s = $_.FullName.replace("[", "``[").replace("]", "``]");
        $log = "Move-Item -Path $s -Destination $n";
        QIALog $log;
        Move-Item -Path "$s" -Destination $n;
    }
}

$now = Get-Date;
$filter = "*.*";
$timespan = new-timespan -minutes 30;
$srcPath = "G:\bcnvision\Qiagen_GMBH\Ln1\Default\Images\0_8_RCs_VISION_CONTROL\VIDI\OK";
$destPath = "\\10.9.15.99\mdfs_new\SSL2\CV\WS17\0_8_RCs_VISION_CONTROL\VIDI\OK\"
MoveItem $srcPath $destPath $filter $timespan;

$filter = "*.bmp";
$timespan = new-timespan -minutes 30;
$srcPath = "G:\bcnvision\Qiagen_GMBH\Ln1\Default\Images\*_RCs_VISION_CONTROL\DM";
RemoveItem $srcPath $filter $timespan;


$filter = "*.bmp";
$timespan = new-timespan -minutes 5;
$srcPath = "G:\bcnvision\Qiagen_GMBH\Ln1\Default\Images\*_RCs_VISION_CONTROL\CAM*";
RemoveItem $srcPath $filter $timespan;

$filter = "*.bcnimg";
$timespan = new-timespan -minutes 10;
$srcPath = "G:\bcnvision\Qiagen_GMBH\Ln1\Default\Images\*_RCs_VISION_CONTROL\CAM*";
RemoveItem $srcPath $filter $timespan;

QIALog "Clean up READY..." 



