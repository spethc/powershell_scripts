﻿<# Mandatory for locating assemblies and basic scripts #>
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath;
}

$root = Get-ScriptDirectory;
Set-Location $root;
$l = Get-Location;
write-host $l;

#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
LoadAssembly ..\assemblies\QIACommon.dll;


$now = Get-Date;
        
$logSettings = New-Object QIACommon.Log.QIALogSettings;
$logSettings.LogDir = "C:\Log";
$logSettings.AppendDate = $true;
$logSettings.FileName = "DeleteJSON_DONE";

[QIACommon.Log.QIALog]::Settings = $logSettings;
QIALog "Starting...";




QIALog("Removing LOG...");
$path = "E:\MDFS\CV\WS13\RawLogs";
$filter = "*.log";
$timespan = new-timespan -days 7
Get-ChildItem $path -Filter $filter -recurse |?{($now - $_.CreationTime) -gt $timespan  }|?{
    #write-host $_.BaseName
    #QIALog "Delete $_.Name";
    Remove-Item $_.fullname;
}



#[System.Windows.MessageBox]::Show('...Done')
QIALog("Removed JSON_DONE");


QIALog "...Done";

