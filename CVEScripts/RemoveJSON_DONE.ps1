﻿QIALog("Removing JSON_DONE...");
$path = "D:\REPORTS\RawLogs";
$filter = "*.json_done";
$timespan = new-timespan -days 7
Get-ChildItem $path -Filter $filter -recurse |?{($now - $_.CreationTime) -gt $timespan  }|?{
    write-host $_.BaseName
    QIALog "Delete $_.Name";
    Remove-Item $_.fullname;
}


$path = "D:\REPORTS\RawLogsPCS";
$filter = "*.json_done";
$timespan = new-timespan -days 7
Get-ChildItem $path -Filter $filter -recurse |?{($now - $_.CreationTime) -gt $timespan  }|?{
    write-host $_.BaseName
    QIALog "Delete $_.Name";
    Remove-Item $_.fullname;
}


#[System.Windows.MessageBox]::Show('...Done')
QIALog("Removed JSON_DONE");
