﻿QIALog("Cleanup C:\Log...");

$path = "C:\Log";
$filter = "*.txt";
$timespan = new-timespan -days 30;
Get-ChildItem $path -Filter $filter -recurse |?{($now - $_.CreationTime) -gt $timespan  }|?{
    write-host $_.BaseName
    QIALog "Delete $_";
    Remove-Item $_.fullname;
}
QIALog("Cleaned up C:\Log...");


$path = "D:\Log";
$filter = "*.txt";
$timespan = new-timespan -days 30;
Get-ChildItem $path -Filter $filter -recurse |?{($now - $_.CreationTime) -gt $timespan  }|?{
    write-host $_.BaseName
    QIALog "Delete $_";
    Remove-Item $_.fullname;
}
QIALog("Cleaned up D:\Log...");
