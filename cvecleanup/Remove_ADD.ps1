﻿#Mandatory for locating assemblies and basic scripts:
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

#Set location
$root = Get-ScriptDirectory
Set-Location $root


#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
. ..\pslib\SQLLib.ps1;
LoadAssemblies;

#Initialize logging:        
$logSettings = New-Object QIACommon.Log.QIALogSettings;
$logSettings.LogDir = "C:\Log";
$logSettings.AppendDate = $true;
$logSettings.FileName = "CVECleanup";

#Initialize Logger with settings:
[QIACommon.Log.QIALog]::Settings = $logSettings;

#Use logger now:
[QIACommon.Log.QIALog]::Log("Initialized logging");
[QIACommon.Log.QIALog]::Log("Starting in $root");

#Simplified usage with QIALog (found in BasicLib.ps1):
QIALog "Starting in $root";


function KillIfUnaligned($file)
{
    $1 = $file.Name;
    $ffn = Get-Item $file.FullName;

    $arr = $1.split("_");
        $m = $1 -match "^SOP-0800-(\d{4})_(\d{8})_(\d{9})_ImageAnalysis_([a-zA-Z0-9]+)_(ADD_MIN_|ADD_PLUS_|ImageAligned_)?(\d{8}T\d{6}).bmp$";
        if ($m -eq $true){


            $org = $matches[0];
            $sop = $matches[1];
            $eqn = $matches[2];
            $snr = $matches[3];
            $net = $matches[4];
            $typ = $matches[5];
            $dtt = $matches[6];


            if($typ -ne "ImageAligned_"){
                [QIACommon.Log.QIALog]::Log("Delete pattern '" + $typ + "': " + $file.FullName);
                write-host $_.Name;
                Remove-Item $_.fullname;
            } else {
                $log = "SOP-0800-" + $sop +"_" + $eqn + "_" + $snr + "_ImageAnalysis_" + $net + "_" + $dtt + ".log"
                $logFound = $false;
                $dir = $file.Directory.FullName;
                Get-ChildItem $dir -Filter $log -recurse |?{$_.PSIsContainer -eq $false }|?{
                    $logFound = $true;
                    #write-host "+++" $log;
                }
                if ($logFound -ne $true){
                    write-host "---" $log;
                    $destPath = $file.Directory.FullName;
                    $destPath = $destPath -replace 'RawLogs', 'NoResultInfo';
                    [QIACommon.Log.QIALog]::Log("Move File '" + $file.Name +"' to " + $destPath);
                    Move-Item -Path $file.FullName -Destination $destPath;
                }
            }


        } 
        #files completely out of interest
        else {
            $lastWrite = $file.LastWriteTime;
            if (($now - $lastWrite) -gt $timespan) {
                $destPath = $file.Directory.FullName;
                $destPath = $destPath -replace 'RawLogs', 'Backup';
                [QIACommon.Log.QIALog]::Log("Move File '" + $file.Name +"' to " + $destPath);
                Move-Item -Path $file.FullName -Destination $destPath;

            }

        } 
}


function MoveTrash($path){

    $dPath = "E:\MDFS\CV\WS02\Backup";
    $ts = new-timespan -days 7;
    Get-ChildItem $path -Filter "*.log" -recurse |?{($now - $_.CreationTime) -gt $ts  }|?{
        write-host $_.Name
#        Remove-Item $_.fullname;
        Move-Item -Path $_.FullName -Destination $dPath;
        [QIACommon.Log.QIALog]::Log("Move File '" + $_.Name +"' to " + $dPath);
    }



    $ts = new-timespan -minutes 20;
    Get-ChildItem $path -Filter "*Not_Align*.*" -recurse |?{($now - $_.CreationTime) -gt $ts  }|?{
        write-host $_.Name
#        Remove-Item $_.fullname;
        Move-Item -Path $_.FullName -Destination $dPath;
        [QIACommon.Log.QIALog]::Log("Move File '" + $_.Name +"' to " + $dPath);
    }


    Get-ChildItem $path -Filter "*_Empty_*.*" -recurse |?{($now - $_.CreationTime) -gt $ts  }|?{
        write-host $_.Name
#        Remove-Item $_.fullname;
        Move-Item -Path $_.FullName -Destination $dPath;
        [QIACommon.Log.QIALog]::Log("Move File '" + $_.Name +"' to " + $dPath);
    }


}



function FileIterator($path){
    QIALog "Clean up $path" 
    Get-ChildItem $path -Filter $filter -recurse |?{$_.PSIsContainer -eq $false }|?{
        KillIfUnaligned $_;
    }
    MoveTrash $path;
}




$now = get-date;
$filter = "*.bmp"
$timespan = new-timespan -minutes 20





FileIterator "E:\MDFS\CV\WS02\RawLogs";
FileIterator "E:\MDFS\CV\WS07\RawLogs";
FileIterator "E:\MDFS\CV\WS13\RawLogs";
FileIterator "E:\MDFSSSL4\CV\WS02\RawLogs";
FileIterator "E:\MDFSSSL4\CV\WS07\RawLogs";
FileIterator "E:\MDFSSSL4\CV\WS13\RawLogs";




