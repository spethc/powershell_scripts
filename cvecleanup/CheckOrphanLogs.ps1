﻿###################################################################################
#
# CheckOrphanLogs.ps1
#
#
# Initials:
# SPETHC Christoph Speth
#
# History:
# 01.01.2021    SPETHC 0.5      Initial Creation
#
# Searches for cve .log - files that do not have a corresponding .bmp file
#
#
####################################################################################




function Init(){

    function Get-ScriptDirectory {
        Split-Path -Parent $PSCommandPath
    }


    # Change directory to source path of this script
    $root = Get-ScriptDirectory
    Set-Location $root
    write-host $root

    . ..\pslib\BasicLib.ps1;

    LoadAssemblies;
}

function IsOrphan($file)
{
    $1 = $file.Name;
    $ffn = Get-Item $file.FullName;
    $arr = $1.split(".");

    # regex mask for the log file
    $m = $1 -match "^SOP-0800-(\d{4})_(\d{8})_(\d{9})_ImageAnalysis_([a-zA-Z0-9]+)_(\d{8}T\d{6}).log$";

    # the file has a valid name
    if ($m -eq $true){

        $1 = $arr[0];
        $arr = $1.split("_");
	    
		$org = $matches[0];
		$sop = $matches[1];
		$eqn = $matches[2];
		$snr = $matches[3];
		$net = $matches[4];
		$typ = $matches[5];
		$dtt = $matches[6];
	
		#Now we do the BMP orphan check:
        $bmpFile = $arr[0] + "_" + $arr[1] + "_" + $arr[2] + "_" + $arr[3] + "_" + $arr[4] + "_ImageAligned_" + $arr[5] + ".bmp";

        $found = $false;
        Get-ChildItem $path -Filter $bmpFile -recurse |?{$_.PSIsContainer -eq $false }|?{
            #write-host "+++" $_.Name;
            $found = $true;
        }
        if ($found -eq $false){
            write-host "---" $bmpFile;

            $lastWrite = $file.LastWriteTime;
            if (($now - $lastWrite) -gt $timespan) {
                $destPath = $file.Directory.FullName;
                $destPath = $destPath -replace 'RawLogs', 'Backup';
                [QIACommon.Log.QIALog]::Log("Move ORPHAN File '" + $file.Name +"' to " + $destPath);
                Move-Item -Path $file.FullName -Destination $destPath;
            }


        }

	
    } 
    #the file has no valid name, so it is trash and moved to backup folder
    else {

        $lastWrite = $file.LastWriteTime;
        if (($now - $lastWrite) -gt $timespan) {
            $destPath = $file.Directory.FullName;
            $destPath = $destPath -replace 'RawLogs', 'Backup';
            [QIACommon.Log.QIALog]::Log("Move NOT_MATCHING File '" + $file.Name +"' to " + $destPath);
            Move-Item -Path $file.FullName -Destination $destPath;
        }

    } 
}


function FileIterator($path){
    write-host $path "######################################################################"
    Get-ChildItem $path -Filter $filter -recurse |?{$_.PSIsContainer -eq $false }|?{
        IsOrphan $_;
    }
}


function main(){
    Init;
    InitLogging "C:\Log" "PSScripting.DeleteUnaligned";

    $now = get-date;
    $filter = "*.log";
    #$filter = "SOP-0800-0015_14008321_300333228_ImageAnalysis_RCNetwork_20201104T155933.log";
    $timespan = new-timespan -days 5
    FileIterator "E:\MDFS\CV\WS13\RawLogs";
}



main;

