﻿#HINTS:
#https://stackoverflow.com/questions/27897669/powershell-remove-files-smaller-than-500bytes-in-directory-recursively
#https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/add-content?view=powershell-7.1

function getRawFiles($name, $filter, $logFile){
    $notFound = $true;
    $i = 0;
    Add-Content -path $logFile -Value "/*processing $name : "
    write-host "/*processing $name : "
    Get-ChildItem $sourcePath -Filter $filter -recurse |?{$_.PSIsContainer -eq $false}|?{
        $notFound = $false;
        Add-Content -path $logFile -Value $_;
        write-host $_;
        $i++;
    }
    Add-Content -path $logFile -Value "$i BMP files for filter '$filter' */";
    write-host "$i BMP files for filter '$filter' */";
    if ($i -gt 0) {
        Add-Content -path $logFile -Value "DELETE FROM CVE1 WHERE SN = $sn;"
        write-host "DELETE FROM CVE1 WHERE SN = $sn;"
    }
}




$destPath = 'E:\MDFS\CV\WS02\2020\12\20'
$sourcePath = 'E:\MDFS\CV\WS02\RawLogs'
$logFile = "E:\MDFS\SpethC\Repair CVE\DELETE_EMPTY_CVE.txt"


Get-ChildItem $destPath -Filter 3003938*.png -recurse |?{$_.PSIsContainer -eq $false -and $_.length -lt 1kB}|?{
#Get-ChildItem $destPath -Filter *.png -recurse |?{$_.PSIsContainer -eq $false -and $_.length -lt 1kB}|?{
    $name = $_.name;
    $arr = $name.split("_");
    $sn = $arr[0];

    $filter = "*" + $sn + "*.bmp";
    $count = getRawFiles $name $filter $logFile;

}
