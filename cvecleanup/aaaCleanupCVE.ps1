﻿function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}





function ProcessCVE($file, $sourcePath, $destPath, $bakPath, $cveTable){
    $cveImage = [QIACommonAdvanced.CVImageBinding]::CreateCVImage(15, $file.Directory, $file.FullName);
    #$found = [QIACommonAdvanced.CVImageBinding]::FindInDB($connectionString, "cve1",  $cveImage.Original);
    #if ($found -eq 1){
       [QIACommonAdvanced.CVImageBinding]::Process($cveImage, $connectionString, $cveTable, $sourcePath, $destPath);
       $bakDest = $bakPath + "\" + $file.Name;
       Move-Item -Path $file.FullName -Destination $bakDest;
    #}
}


function ProcessSourceFiles($filePath, $destPath, $bakPath, $filter, $cveTable){
    Get-ChildItem $filePath -Filter $filter -recurse |?{$_.PSIsContainer -eq $false}|?{
        ProcessCVE $_ $filePath $destPath $bakPath $cveTable; 
    }
}



function ProcessSmallFiles($execute) {
<#
Usecase: However, small and invalid PNGs were produced.
So we filtered against the small files as criteria and tracked this back to the original BMP image and ran the CVE import once more
#>

    $i = 0;
    Get-ChildItem $checkPath -Filter $basicFilter -recurse |?{$_.PSIsContainer -eq $false -and $_.length -lt 1kB}|?{
        $name = $_.name;
        $arr = $name.split("_");
        $sn = $arr[0];
        $dateTime = $arr[1];
        $dateTime = $dateTime.Replace("-", "T");
        $filter = "*" + $sn + "*" + $dateTime + "*.bmp";
        $i = $i + 1;
        # SpoolObject $_;
        write-host $i "--- " $_.Name " --------------------";
        if ($execute -eq 1){
            $count = ProcessSourceFiles $sourcePath $destPath $bakPath $name $filter $cveTable;
        }
        write-host "";
    }
    write-host "Ready with $basicFilter"
    $DUMMY = Read-Host -Prompt 'Press <ENTER> to exit...'
}


function ProcessCVE1(){
    $connectionString = "Server=10.9.15.99;Port=3306;Database=qiastatdx;Uid=qia;Pwd=statdx";
    $destPath = 'E:\MDFS\CV\WS02';
    $bakPath = 'E:\MDFS\CV\WS02\Backup';
    $checkPath =  $destPath + "\2020\12\21";
    $sourcePath = 'E:\MDFS\CV\WS02\RawLogs';
    $basicFilter = "*300499*_ImageAnalysis_FluidicNetwork_ImageAligned_*.bmp";
    $cveTable = "cve1";
    #ProcessSmallFiles 0;
    ProcessSourceFiles $sourcePath $destPath $bakPath $basicFilter $cveTable
}


function snFileCheck($sn){
    $checkPath = 'E:\MDFS\CV\WS07';
    $basicFilter = $sn + "*.png";
    Get-ChildItem $checkPath -Filter $basicFilter -recurse |?{$_.PSIsContainer -eq $false}|?{
        write-host $_.Name;
    }
}


function GetChildren($p1, $fil) {
    #write-host $p1;
    #Get-ChildItem $p1 -Filter "300394719_20201220-195109_ALIGNED.png" -recurse |?{$_.PSIsContainer -eq $false}|?{
    Get-ChildItem $p1 -Filter $fil -recurse |?{$_.PSIsContainer -eq $false}|?{
       write-host $_.length $_.Name;
    }
}



function DisplayRecovered(){
    $connectionString = "Server=10.9.15.99;Port=3306;Database=qiastatdx;Uid=qia;Pwd=statdx";

    $sql = "SELECT
  lsn.SN,
  CVE1.path AS CVE1_path,
  CVE1.file AS CVE1_file,
  CVE2.path AS CVE2_path,
  CVE2.file AS CVE2_file,
  CVE3.path AS CVE3_path,
  CVE3.file AS CVE3_file
 FROM LABELSN lsn
 LEFT OUTER JOIN  CVE1 ON CVE1.SN = lsn.SN
 LEFT OUTER JOIN  CVE2 ON CVE2.SN = lsn.SN
 LEFT OUTER JOIN  CVE3 ON CVE3.SN = lsn.SN
 /*WHERE CVE1.path = '2020\\12\\20'*/
 WHERE lsn.SN  =    "
 
 $arr = SQLToArray $connectionString  $sql;
 $arr | ForEach-Object -Process {
 
    $p1 = "E:\MDFS\CV\WS02\" + $_.CVE1_path;
    $p2 = "E:\MDFS\CV\WS07\" + $_.CVE2_path;
    $p3 = "E:\MDFS\CV\WS13\" + $_.CVE3_path;
    $file1 = $p1 + "\" + $_.CVE1_file;
    $file2 = $p2 + "\" + $_.CVE2_file;
    $file3 = $p3 + "\" + $_.CVE3_file;

    SpoolObject $_;
    $fil = $_.CVE1_file;
    if ($fil  -eq $null){
    }
    else {
        GetChildren $p1 $fil;
        }
 }
 $arr |Format-Table;

}


$root = Get-ScriptDirectory
Set-Location $root



#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
. ..\pslib\SQLLib.ps1;
LoadAssemblies;

#DisplayRecovered;

InitLogging "C:\Log" "CVE1Cleanup_NEW"

ProcessCVE1;



$a = read-host 



