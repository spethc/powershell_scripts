﻿#Mandatory for locating assemblies and basic scripts:
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

#Set location
$root = Get-ScriptDirectory
Set-Location $root


#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
LoadAssemblies;

#Initialize logging:        
$logSettings = New-Object QIACommon.Log.QIALogSettings;
$logSettings.LogDir = "C:\log";
$logSettings.AppendDate = $true;
$logSettings.FileName = "Push2MDFS";

#Initialize Logger with settings:
[QIACommon.Log.QIALog]::Settings = $logSettings;


#Simplified usage with QIALog (found in BasicLib.ps1):
QIALog "'#########################################"
QIALog "Starting in $root";

$now = get-date;
$timespan = new-timespan -minutes 20;
$source = "\\10.100.230.143\reports\RawLogs";
$dest = "E:\mdfs\CV\WS13\RawLogs";
$env = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name;
$envMsg = "running with identity " + $env;
QIALog $envMsg;

#We create a network folder mapping (that may already exists)
#https://it-learner.de/windows-netzlaufwerk-mit-der-powershell-verbinden/
$password = ConvertTo-SecureString 'Barca99!' -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential ('10.100.230.143\Engineer', $password)
new-psdrive -Root \\10.100.230.143\Reports\RawLogs -Name Y -Credential $credential -PSProvider FileSystem;


# The BMP files with "ADD_" in the name are useless, we delete them:
$filter = "*ADD_*.bmp";
$destMsg = "Deleting files from " + $source + ", filter=" + $filter;
QIALog $destMsg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $i = $i + 1;
    Remove-Item -Path $_.FullName;
    QIALog "Delete $_"
}
$msg = "ready with deleting " + $i + " files..."
QIALog $msg;


# The BMP files with "_Not_Aligned*" in the name are useless, we delete them:
$filter = "*_Not_Aligned*.bmp";
$destMsg = "Deleting files from " + $source + ", filter=" + $filter;
QIALog $destMsg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $i = $i + 1;
    Remove-Item -Path $_.FullName;
    QIALog "Delete $_"
}
$msg = "ready with deleting " + $i + " files..."
QIALog $msg;


# The BMP+LOG files with "_Not_Aligned*" in the name are useless, we delete them:
$filter = "*_Empty_ImageAnalysis_*.*";
$destMsg = "Deleting files from " + $source + ", filter=" + $filter;
QIALog $destMsg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $i = $i + 1;
    Remove-Item -Path $_.FullName;
    QIALog "Delete $_"
}
$msg = "ready with deleting " + $i + " files..."
QIALog $msg;




# The *.json_done files keep the metadata information and are renamed to *.log:
$filter = "*.json_done";
$destMsg = "Moving files from " + $source + " to " + $dest + " with filter '" + $filter + "'";
QIALog $destMsg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $newName = $dest + "\" + $_.BaseName + ".log";
    QIALog ("Move-Item -Path " + $_.FullName + " -Destination " + $newName);
    $i = $i + 1;
    Move-Item -Path $_.FullName -Destination $newName -Force;
}
$msg = "ready with moving " + $i + " files..."
QIALog $msg;

# The rest of the BMP files we copy:
$filter = "*.bmp";
$destMsg = "Moving files from " + $source + " to " + $dest + " with filter '" + $filter + "'";
QIALog $destMsg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $newName = $dest + "\" + $_.BaseName + ".bmp";
    QIALog ("Move-Item -Path " + $_.FullName + " -Destination " + $newName);
    $i = $i + 1;
    Move-Item -Path $_.FullName -Destination $newName -Force;
}
$msg = "ready with moving " + $i + " files..."
QIALog $msg;


QIALog "'#########################################"

