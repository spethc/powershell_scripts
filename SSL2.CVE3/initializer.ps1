﻿<# Mandatory for locating assemblies and basic scripts #>
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

$root = Get-ScriptDirectory
Set-Location $root

#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
LoadAssembly ..\assemblies\QIACommon.dll;


$now = Get-Date;
        
$logSettings = New-Object QIACommon.Log.QIALogSettings;
$logSettings.LogDir = "C:\Log";
$logSettings.AppendDate = $true;
$logSettings.FileName = "DataTransfer_";

[QIACommon.Log.QIALog]::Settings = $logSettings;

#Simplified usage with QIALog (found in BasicLib.ps1):
QIALog "'#########################################"
QIALog "Starting in $root";



. .\cleanupLogs.ps1
. .\transferCVEsToMDFS.ps1

QIALog "...Done";

