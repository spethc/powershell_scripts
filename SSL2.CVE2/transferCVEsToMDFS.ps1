﻿#############################################################################
# Variables & Constants:
#############################################################################
$target = "\\10.9.15.99\mdfs\CV\WS07\RawLogs\"
$source = "D:\reports\RawLogs";
$sourcePCS = "D:\reports\RawLogsPCS";
$timespan = new-timespan -minutes 20;
$destDrive = "M"
$dest = $destDrive + ":";
$now = get-date;


#We create a network folder mapping (that may already exists)
#https://it-learner.de/windows-netzlaufwerk-mit-der-powershell-verbinden/
$password = ConvertTo-SecureString 'Welcome@123' -AsPlainText -Force;
$credential = New-Object System.Management.Automation.PSCredential ('QIAGEN\svc_statsslws2', $password);
new-psdrive -Root $target -Name $destDrive -Credential $credential -PSProvider FileSystem;

$env = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name;
$msg = "running with identity " + $env;
QIALog $msg;


#############################################################################
# Run:
#############################################################################



# The BMP files with "ADD_" in the name are useless, we delete them:
$filter = "*ADD_*.bmp";
$msg = "Deleting files from " + $source + ", filter=" + $filter;
QIALog $msg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $i = $i + 1;
    Remove-Item -Path $_.FullName;
    QIALog "Delete $_"
}
$msg = "ready with deleting " + $i + " files..."
QIALog $msg;
#############################################################################
    
# The BMP files with "_Not_Aligned*" in the name are useless, we delete them:
$filter = "*_Not_Aligned*.bmp";
$msg = "Deleting files from " + $source + ", filter=" + $filter;
QIALog $msg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $i = $i + 1;
    Remove-Item -Path $_.FullName;
    QIALog "Delete $_";
}
$msg = "ready with deleting " + $i + " files...";
QIALog $msg;
#############################################################################


# The BMP+LOG files with "_Not_Aligned*" in the name are useless, we delete them:
$filter = "*_Empty_ImageAnalysis_*.*";
$msg = "Deleting files from " + $source + ", filter=" + $filter;
QIALog $msg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $i = $i + 1;
    Remove-Item -Path $_.FullName;
    QIALog "Delete $_";
}
$msg = "ready with deleting " + $i + " files...";
QIALog $msg;
#############################################################################


# The *.json_done files keep the metadata information and are renamed to *.log:
$filter = "*.json_done";
$msg = "Moving files from " + $source + " to M:\ with filter '" + $filter + "'";
QIALog $msg;
$i = 0;
Get-ChildItem $sourcePCS -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $newName = $dest + "\" + $_.BaseName + ".log";
    QIALog ("Move-Item -Path " + $_.FullName + " -Destination " + $newName);
    $i = $i + 1;
    Move-Item -Path $_.FullName -Destination $newName -Force;
}
$msg = "ready with moving " + $i + " files...";
QIALog $msg;
#############################################################################


# The rest of the BMP files we copy:
$filter = "*.bmp";
$destMsg = "Moving files from " + $source + " to " + $dest + " with filter '" + $filter + "'";
QIALog $destMsg;
$i = 0;
Get-ChildItem $source -Filter $filter -recurse |?{$_.PSIsContainer -eq $false  -and ($now - $_.CreationTime) -gt $timespan }|?{
    $newName = $dest + "\" + $_.BaseName + ".bmp";
    QIALog ("Move-Item -Path " + $_.FullName + " -Destination " + $newName);
    $i = $i + 1;
    Move-Item -Path $_.FullName -Destination $newName -Force;
}
$msg = "ready with moving " + $i + " files...";
QIALog $msg;


QIALog "'#########################################"

