﻿<# Mandatory for locating assemblies and basic scripts #>
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

$root = Get-ScriptDirectory
Set-Location $root

#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
LoadAssembly ..\assemblies\QIACommon.dll;


$now = Get-Date;
        
$logSettings = New-Object QIACommon.Log.QIALogSettings;
$logSettings.LogDir = "C:\Log";
$logSettings.AppendDate = $true;
$logSettings.FileName = "CleanupVision_";

[QIACommon.Log.QIALog]::Settings = $logSettings;
QIALog "Starting...";
. .\cleanupCdriveLog.ps1
. .\transferImagesToMDFS.ps1

QIALog "...Done";

