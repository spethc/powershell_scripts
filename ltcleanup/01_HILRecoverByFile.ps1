﻿#Mandatory for locating assemblies and basic scripts:
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

#Set location
$root = Get-ScriptDirectory
Set-Location $root



function ProcessRegisterFile($sourcePath, $sourceFile) {
	$i = 0;






	[System.Collections.ArrayList]$ali = @();
    $f = $sourcePath + "\" + $sourceFile
	Get-Content -Path $f |?{
		$arr = $_.split(",");
		if ($arr[0] -ne "Test_number") {
            $regIndex = $arr[0];
            $module = $arr[3];
			$sn = $arr[5];
			$regPath = $resultBase + "\regDataT" + $module;
			$rawPath = $resultBase + "\rawDataT" + $module;
            $csvDonefile = $arr[9] +  "_done";
			$RegDataFile = $regPath + "\" + $arr[9] + "_done";
			$RawDataFile = $rawPath + "\" + $arr[10];

			if (($sn -ne "ReaderOFF") -and ($sn -ne $null)){
				$sql = "SELECT 1, sn, equipment FROM leaktest WHERE sn = '" + $sn + "' AND RegDataFile = '" + $arr[9] + "' AND RegisterFile = '" + $sourceFile + "_'";
				$hit = SQLToArray $connectionStringMDFS $sql
				if ($hit.Count -eq 0)
				{
                    write-host "Missing entry for RegIdx=$regIndex";
					$i = $i + 1;
                    $ob = New-Object PsObject;
					$ob | Add-Member NoteProperty -Name _Idx -Value $i;
                    $ob | Add-Member NoteProperty -Name RegisterIndex -Value $regIndex;
					$ob | Add-Member NoteProperty -Name IndexFile -Value $sourceFile;
					$ob | Add-Member NoteProperty -Name Equipment -Value $equipment.Name;
					$ob | Add-Member NoteProperty -Name Test_number -Value $arr[0];
					$ob | Add-Member NoteProperty -Name Date -Value $arr[1];
					$ob | Add-Member NoteProperty -Name Start_time -Value $arr[2];
					$ob | Add-Member NoteProperty -Name Module -Value $module;
					$ob | Add-Member NoteProperty -Name SN -Value $arr[5];
					$ob | Add-Member NoteProperty -Name Result -Value $arr[6];
					$ob | Add-Member NoteProperty -Name RegDataFile -Value $csvDonefile;
					$ob | Add-Member NoteProperty -Name RegDataFileFound -Value False;
					$ob | Add-Member NoteProperty -Name RegDataFileSize -Value 0;
					$ob | Add-Member NoteProperty -Name RawDataFile -Value $arr[10];
					$ob | Add-Member NoteProperty -Name RawDataFileFound -Value "False";
					$ob | Add-Member NoteProperty -Name RawDataFileSize -Value 0;

					if ($module -ne $null){
					    if ($arr[9] -ne $null){
							try { 
								Get-Item $RegDataFile -ErrorAction Stop | ?{
									$ob.RegDataFileFound = "True";
									$ob.RegDataFileSize = $_.Length;


<#
                                    [QIACommonAdvanced.LeakTestFileTransfer]::ImportByRegFile(
                                        $connectionStringMDFS, 
                                        $sourcePath, $sourceFile,
                                        $regPath, $arr[9],
                                        $equipment.EquipmentType, ",");
                                        #>


								}
							} catch {
								$ob.RegDataFileFound = "False";
							}
						} 
                         
						if ($arr[10] -ne $null){
							try{
								Get-Item $RawDataFile -ErrorAction Stop | ?{
									$ob.RawDataFileFound = "True";
									$ob.RawDataFileSize = $_.Length;
								}
							} catch {
								$ob.RawDataFile = $null;
							}
						}

					} 
                    
                    $ltSQL =                         "select 
                    	    LT.ID AS LT_ID,
	                        (select COUNT(ID) FROM leaktest_reg_steps lts2 WHERE lts2.RegID = ltr.ID) AS STEP_COUNT,
                            lt.RegDataFile
                        from leaktest lt
                        left outer join leaktest_reg ltr ON ltr.LeakTestID = lt.ID
                        left outer join  leaktest_reg_steps lts ON lts.RegID = ltr.id 
                        WHERE
                            lt.RegisterFile = '" + $ob.IndexFile + "' AND
                            lt.RegDataFile = '" + $ob.RegDataFile + "' 
                        LIMIT 1";
                    $hitLT = SQLToArray $connectionStringLT $ltSQL
   					$ob | Add-Member NoteProperty -Name FoundInEqDB -Value ($hitLT.Length -ne 0);
                    if ($hitLT.Length -ne 0){
    					$ob | Add-Member NoteProperty -Name LTIdxEqDB -Value $hitLT[1].LT_ID;
	    				$ob | Add-Member NoteProperty -Name ValCountEqDB -Value $hitLT[1].STEP_COUNT;
                    } else {
    					$ob | Add-Member NoteProperty -Name LTIdxEqDB -Value $null;
	    				$ob | Add-Member NoteProperty -Name ValCountEqDB -Value 0;
                    }
					    $ali.Add($ob);
				} 
			}
		}
	}
    #if ($ali.Count -gt 0){
	    array2CSV $ali $sourcePath ("CHECK_AFTER_IMPORT_$sourceFile");
    #}
}


###########################################################################

#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
. ..\pslib\SQLLib.ps1;
LoadAssemblies;

InitLogging "LTFileRecover"

$HIL1 = New-Object PsObject;
$HIL1 | Add-Member NoteProperty -Name Name -Value "HIL1";
$HIL1 | Add-Member NoteProperty -Name IP -Value 10.100.48.35;
$HIL1 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFSSSL4\LTRecovery\TestRegister_HIL1";
$HIL1 | Add-Member NoteProperty -Name EquipmentType -Value "HIL1_SSL4"
   

$HIL5 = New-Object PsObject;
$HIL5 | Add-Member NoteProperty -Name Name -Value "HIL5";
$HIL5 | Add-Member NoteProperty -Name IP -Value 10.100.48.36;
$HIL5 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFSSSL4\LTRecovery\TestRegister_HIL5";
$HIL5 | Add-Member NoteProperty -Name EquipmentType -Value "HIL5_SSL4"

$HIL6 = New-Object PsObject;
$HIL6 | Add-Member NoteProperty -Name Name -Value "HIL6";
$HIL6 | Add-Member NoteProperty -Name IP -Value 10.100.48.37;
$HIL6 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFSSSL4\LTRecovery\TestRegister_HIL6";
$HIL6 | Add-Member NoteProperty -Name EquipmentType -Value "HIL6_SSL4"

$HIL11 = New-Object PsObject;
$HIL11 | Add-Member NoteProperty -Name Name -Value "HIL11";
$HIL11 | Add-Member NoteProperty -Name IP -Value 10.100.230.151;
$HIL11 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFS\LTRecovery\TestRegister_HIL11";
$HIL11 | Add-Member NoteProperty -Name EquipmentType -Value "HIL11"


#======================= SETTINGS
$equipment = $HIL11
$db = "qiastatdx"
$filter = "IPC_TestRegister_*.csv"
#======================= /SETTINGS

$connectionStringMDFS = "Server=10.9.15.99;Port=3306;Database=" + $db + ";Uid=root;Pwd=Barca99!";
$connectionStringLT = "Server=" + $equipment.IP + ";Port=3306;Database=leaktestdb;Uid=mdfs;Pwd=statdx";
$resultBase = "\\" + $equipment.IP + "\Result";

Remove-SmbMapping -LocalPath "V:" -Force
New-SmbMapping -LocalPath 'V:' -RemotePath $resultBase



Get-ChildItem -Path $equipment.SourcePath -Filter $filter | ?{ 
    ProcessRegisterFile $equipment.SourcePath $_.Name
}

Remove-SmbMapping -LocalPath "V:" -Force
write-host "READY..."
$a = read-host