﻿<#
Script that looks for invalid Leak Test Data.
Invalid means, that the leaktest_reg_steps have not been written.
This only works for leaktesters wit an onw database, not for those with file access.

The data is fetched from the equipment database.

#>
function IterateUpdate($arr){
    $i = 0;
    $arr| ForEach-Object{ 
        if ($_.RegdataFile -ne $null) {
            $i = $i + 1;
            $i;
            QIALog "==============";
            $s = "ID=" + $_.ID + ";SN=" + $_.SN + "; RegisterIndex=" + $_.RegisterIndex + ";RegDataFile=" + $_.RegDataFile + ";RegID=" + $_.reg_id;
            QIALog $s;
            $file = $_.RegDataFile;
            $stepCount = $_.STEP_COUNT;
            $s = "updating " + $file + " with " + $stepCount + " records";
            QIALog $s;
            $ltt = New-Object QIACommonAdvanced.LeakTestDBTransfer("HIL", $eqID, $connectionStringLT, $connectionStringMDFS);
            QIALog $file;
            #$ltt.UpdateLeaktestInDB($file);
            $ltt.InsertLeaktestToDB($file);
        }
    }
}



. C:\DataDeputy\BasicLib.ps1;
. C:\DataDeputy\SQLLib.ps1;

LoadAssemblies;


$connectionStringMDFS = "Server=10.9.15.99;Port=3306;Database=qiastatdxssl4;Uid=root;Pwd=Barca99!";
$connectionStringLT = "Server=10.100.48.36;Port=3306;Database=leaktestdb;Uid=mdfs;Pwd=statdx";


#Equipment ID, =1 for HIL1...:
$eqID = 5;
#start of the reparation interval:
$startDate = "'2020-12-08 00:00:00'";
#send of the reparation interval:
$endDate = "'2021-01-21 00:00:00'";
#limit to maxCount records:
$maxrecCount = 1;
#define log file:
$logFile = "PSScripting.LeakTestRecovery_JAN_2021"
#define log folder:
$logFolder = "C:\Log" 


$sql="select ID, Equipment, SN, RegisterIndex, RegDataFile, reg_id, STEP_COUNT from LEAKTEST_ERROR_VIEW WHERE RegDataFile IN
(
'RgD_20200817_174415_ID300180518.csv'
) LIMIT " + $maxrecCount;


InitLogging $logFolde $logFile;
QIALog "==============";

<#
QIALog $sql;
$arr = SQLToArray $connectionStringMDFS $sql;
$arr |Format-Table;
IterateUpdate $arr
#>

$ltt = New-Object QIACommonAdvanced.LeakTestDBTransfer("HIL", $eqID, $connectionStringLT, $connectionStringMDFS);
QIALog $file;
#$ltt.UpdateLeaktestInDB($file);
$ltt.InsertLeaktestToDB("RgD_20210122_112209_ID315045767.csv");
$a = read-host 


<#
View for Leak Test and dependant data:

CREATE OR REPLACE VIEW LEAKTEST_ERROR_VIEW AS
select 
   /*(select count(*) from leaktest lt2 where lt2.regDataFile = lt.regDataFile) AS COUNTERREG,*/
    lt.*,
    /*
	lt.ID AS LEAKTEST_ID,
	lt.Equipment,
	lt.SN,
	lt.timestamp,
	lt.RegDataFile,
	*/
	(select ID FROM leaktest_reg ltr2 WHERE ltr2.LeakTestID = lt.ID) AS REG_ID,
	(select COUNT(ID) FROM leaktest_reg_steps lts2 WHERE lts2.RegID = ltr.ID) AS STEP_COUNT
from leaktest lt
left outer join leaktest_reg ltr ON ltr.LeakTestID = lt.ID
left outer join  leaktest_reg_steps lts ON lts.RegID = ltr.id
;


select * from LEAKTEST_ERROR_VIEW
WHERE STEP_COUNT <> 100 AND EQUIPMENT='HIL4' AND DATE(timestamp) > '2020-12-18 00:00:00' AND DATE(timestamp) < '2020-12-23 00:00:00' ORDER BY ID LIMIT 10


/*
select * from LEAKTEST_ERROR_VIEW
WHERE STEP_COUNT <> 100 AND EQUIPMENT='HIL4' AND DATE(timestamp) > '2020-12-18 00:00:00' AND DATE(timestamp) < '2020-12-23 00:00:00' ORDER BY ID LIMIT 10

select * from LEAKTEST_ERROR_VIEW
WHERE RegdataFile = 'RgD_20201219_000430_ID300392196.csv'
*/

select * from LEAKTEST_ERROR_VIEW
WHERE STEP_COUNT <> 100 AND EQUIPMENT='HIL4' AND DATE(timestamp) > '2020-12-18 00:00:00' AND DATE(timestamp) < '2020-12-23 00:00:00' ORDER BY ID LIMIT 10


#>

