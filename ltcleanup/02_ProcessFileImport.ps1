﻿
#Mandatory for locating assemblies and basic scripts:
function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

function IterateFile ($path, $file){
    $file = $path + "\" + $file;
    $CSV = Import-CSV -Path $file -Delimiter "`t";
    $CSV | Where-Object { 
            ($_.RegDataFileFound -eq $True) -and 
            #($_.FoundInEqDB -eq $False) -and 
            #($_.RegDataFile -eq "RgD_20210114_164942_ID315036978.csv") -and
            (1 -eq 1)
        } |Foreach-Object{

        $sourcePath = $equipment.SourcePath;
        $sourceFile = $_.IndexFile;
        $regPath = $resultBase;
        $regFile = $_.RegDataFile;
        QIALog regPath=$regPath

        write-host "1:"$connectionStringMDFS;
        write-host "2:"$sourcePath $sourceFile;
        write-host "3:"$regPath $regFile;
        write-host "4:"$equipment.EquipmentType;
        [QIACommonAdvanced.LeakTestFileTransfer]::ImportByRegFile(
            $connectionStringMDFS, 
            $sourcePath, $sourceFile,
            $regPath, $regFile,
            $equipment.EquipmentType, ","
         );

    }
}


#Set location
$root = Get-ScriptDirectory
Set-Location $root


#Load libraries & assemblies:
. ..\pslib\BasicLib.ps1;
. ..\pslib\SQLLib.ps1;
LoadAssemblies;

InitLogging "C:\Log" "LTFileRecover"
QIALog "HUI"

$HIL1 = New-Object PsObject;
$HIL1 | Add-Member NoteProperty -Name Name -Value "HIL1";
$HIL1 | Add-Member NoteProperty -Name IP -Value 10.100.48.35;
$HIL1 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFSSSL4\LTRecovery\TestRegister_HIL1";
$HIL1 | Add-Member NoteProperty -Name EquipmentType -Value "HIL1_SSL4"
   

$HIL5 = New-Object PsObject;
$HIL5 | Add-Member NoteProperty -Name Name -Value "HIL5";
$HIL5 | Add-Member NoteProperty -Name IP -Value 10.100.48.36;
$HIL5 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFSSSL4\LTRecovery\TestRegister_HIL5";
$HIL5 | Add-Member NoteProperty -Name EquipmentType -Value "HIL5_SSL4"

$HIL6 = New-Object PsObject;
$HIL6 | Add-Member NoteProperty -Name Name -Value "HIL6";
$HIL6 | Add-Member NoteProperty -Name IP -Value 10.100.48.37;
$HIL6 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFSSSL4\LTRecovery\TestRegister_HIL6";
$HIL6 | Add-Member NoteProperty -Name EquipmentType -Value "HIL6_SSL4"

$HIL11 = New-Object PsObject;
$HIL11 | Add-Member NoteProperty -Name Name -Value "HIL11";
$HIL11 | Add-Member NoteProperty -Name IP -Value 10.100.230.151;
$HIL11 | Add-Member NoteProperty -Name SourcePath -Value "E:\MDFS\LTRecovery\TestRegister_HIL11";
$HIL11 | Add-Member NoteProperty -Name EquipmentType -Value "HIL11"



#======================= SETTINGS
$equipment = $HIL11
$db = "qiastatdx"
$filter = "CHECK_AFTER_*.csv"
#======================= /SETTINGS


$connectionStringMDFS = "Server=10.9.15.99;Port=3306;Database=" + $db + ";Uid=root;Pwd=Barca99!";
$connectionStringLT = "Server=" + $equipment.IP + ";Port=3306;Database=leaktestdb;Uid=mdfs;Pwd=statdx";
$resultBase = "\\" + $equipment.IP + "\Result";

#Remove-SmbMapping -LocalPath "X:" -Force
#New-SmbMapping -LocalPath 'X:' -RemotePath $resultBase


Get-ChildItem -Path $equipment.SourcePath -Filter $filter | ?{ 
    IterateFile $equipment.SourcePath $_.Name
}

#Remove-SmbMapping -LocalPath "X:" -Force

$a = read-host

