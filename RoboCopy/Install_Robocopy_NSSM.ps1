﻿$nssm = (Get-Command nssm).Source
$serviceName = 'Cleanup MDFSSSL4'
$powershell = (Get-Command powershell).Source
$scriptPath = 'C:\PowerShellScripts\RoboCopy\CleanupEDrive_MDFS.ps1'
$arguments = '-ExecutionPolicy Bypass -NoProfile -File "{0}"' -f $scriptPath
& $nssm install $serviceName $powershell $arguments
& $nssm status $serviceName
Start-Service $serviceName
Get-Service $serviceName
